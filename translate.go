package rikaku_translate

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"cloud.google.com/go/translate"
	"golang.org/x/text/language"
)

type TranslationRequestData struct {
	TargetLanguages []string `json:"target_languages"`
	Text            string   `json:"text"`
}

type BatchTranslationRequestData struct {
	TargetLanguages []string `json:"target_languages"`
	Texts           []string `json:"texts"`
}

type Result struct {
	EnglishDefinition    string `json:"en_definition"`
	JapaneseDefinition   string `json:"ja_definition"`
	VietnameseDefinition string `json:"vi_definition"`
}

type LanguageDefinition struct {
	Source     string `json:"source"`
	Translated string `json:"translated"`
	Key        string `json:"key"`
}

type BatchResult struct {
	Definitions []LanguageDefinition `json:"definitions"`
}

func TranslateText(w http.ResponseWriter, r *http.Request) {
	// Set CORS headers for the preflight request
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")

	ctx := context.Background()

	var data TranslationRequestData

	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	client, err := translate.NewClient(ctx)
	if err != nil {
		http.Error(w, fmt.Sprintf("translate.NewClient: %v", err.Error()), http.StatusBadRequest)

		return
	}
	defer client.Close()
	result := Result{}

	for _, l := range data.TargetLanguages {
		lang, err := language.Parse(l)
		if err != nil {
			http.Error(w, fmt.Sprintf("language.Parse: %v", err.Error()), http.StatusBadRequest)

			return
		}
		resp, err := client.Translate(ctx, []string{data.Text}, lang, nil)
		if err != nil {
			http.Error(w, fmt.Sprintf("client.Translate: %v", err.Error()), http.StatusUnprocessableEntity)

			return
		}

		if len(resp) == 0 {
			http.Error(w, fmt.Sprintf("There is no translations for %s in %s", data.Text, l), http.StatusInternalServerError)

			return
		}

		switch l {
		case "en":
			result.EnglishDefinition = resp[0].Text
		case "ja":
			result.JapaneseDefinition = resp[0].Text
		case "vi":
			result.VietnameseDefinition = resp[0].Text
		}
	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(result)
}

func TranslateTextInBatch(w http.ResponseWriter, r *http.Request) {
	// Set CORS headers for the preflight request
	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")

	ctx := context.Background()

	var data BatchTranslationRequestData

	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	client, err := translate.NewClient(ctx)
	if err != nil {
		http.Error(w, fmt.Sprintf("translate.NewClient: %v", err.Error()), http.StatusBadRequest)

		return
	}
	defer client.Close()
	result := BatchResult{}

	for _, l := range data.TargetLanguages {
		lang, err := language.Parse(l)
		if err != nil {
			http.Error(w, fmt.Sprintf("language.Parse: %v", err.Error()), http.StatusBadRequest)

			return
		}
		resp, err := client.Translate(ctx, data.Texts, lang, nil)
		if err != nil {
			http.Error(w, fmt.Sprintf("client.Translate: %v", err.Error()), http.StatusUnprocessableEntity)

			return
		}

		if len(resp) == 0 {
			http.Error(w, fmt.Sprintf("There is no translations for %v in %s", data.Texts, l), http.StatusInternalServerError)

			return
		}

		for i, text := range data.Texts {
			result.Definitions = append(
				result.Definitions,
				LanguageDefinition{Source: text, Translated: resp[i].Text, Key: fmt.Sprintf("%s_definition", l)},
			)
		}
	}

	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(result)
}
