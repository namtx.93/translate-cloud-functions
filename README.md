# translate-cloud-functions

### How to deploy to Google Cloud Function

```bash
$ gcloud functions deploy translate --entry-point TranslateText --trigger-http  --runtime go113 --allow-unauthenticated
```

### How to translate a text

```bash
curl --location --request POST 'https://$YOUR_REGION-$YOUR_PROJECT_ID.cloudfunctions.net/translate' \
--header 'Content-Type: application/json' \
--data-raw '{
    "target_language": "ja",
    "text": "Xin chào"
}'
```
