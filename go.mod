module gitlab.com/namtx.93/translate-cloud-functions

go 1.15

require (
	cloud.google.com/go/translate v0.1.0
	golang.org/x/text v0.3.6
)
